# README

Also read the **TASKS.md** file for the job's tasks.

## Running

Run the project by calling `npm run dev`.

## Project structure

All the files names are in **PascalCase**, while the folders names are in **snake_case**.

-   **public** - the static files of the project
-   **src** - all the project source files are placed here;
    -   **components** - the React components that are components part of one page;
    -   **pages** - the React pages composed of multiple **components**;
    -   **styles** - the SASS style files where the name;
        -   **global.scss** - the global style file that is imported in **App.tsx**;
    -   **modules** - multiple parts of the application;
        -   **...[module name]**
            -   **components** - the React components that are components part of one page;
            -   **pages** - the React pages composed of multiple **components**;
            -   **styles** - the SASS style files where the name;
                -   **components** - the style files for the components files where the name of the file is the name of the component;
                -   **pages** - the style files for the pages files where the name of the file is the name of the component;
            -   **utils** - the module utility functions files;
    -   **utils** - the directory containing all the general utility function files;
-   **.eslintrc.json** - the ESLint configuration file;
-   **.gitignore** - the Git ignore file;
-   **.prettierrc.json** - the Prettier configuration file;
-   **package.json** - the NPM package configuration;
-   **README.md** - this file
-   **tailwind.config.ts** - the TailwindCSS configuration file;
-   **TASKS.md** - the job tasks;
-   **tsconfig.json** - the general TypeScript configuration file;

## File structure

### React components & pages

-   the React components and pages should be defined as functional components;
-   the React components and pages should be exported as default.

### CSS styles

-   the name of the classes are in **kebab-case**;
-   the name of the IDs are in **PascalCase**.

## Packages used

-   **NextJS**
-   **ReactJS**
-   **TailwindCSS**
-   **TypeScript**
